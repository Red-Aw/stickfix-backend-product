<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'priceInMinorUnit',
        'category',
        'isActive',
        'state',
        'brand',
        'size',
        'stickSize',
        'stickFlex',
        'bladeCurve',
        'bladeSide',
        'skateLength',
        'skateWidth',
        'createdAt',
        'editedAt',
    ];

    public function images()
    {
        return $this->hasMany(Image::class, 'itemId');
    }
}
