<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'itemId',
        'offerId',
        'name',
        'path',
        'createdAt',
        'editedAt',
    ];

    public function item()
    {
        $this->belongsTo(Item::class, 'id');
    }
}
