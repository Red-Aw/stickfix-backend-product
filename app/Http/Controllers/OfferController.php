<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Offer;

use App\Http\Controllers\ReCaptcha3;
use App\Http\Controllers\ImageController;

use Carbon\Carbon;

class OfferController extends Controller
{
    public function getOffers($isAuth)
    {
        try {
            if ($isAuth) {
                $offers = Offer::with('images')->orderBy('createdAt', 'desc')->get();
            } else {
                if (Cache::has('getOffersGuest')) {
                    $offers = Cache::get('getOffersGuest');
                } else {
                    $offers = Offer::with('images')->where('isActive', true)->orderBy('createdAt', 'desc')->get();
                    Cache::put('getOffersGuest', $offers, Carbon::now()->addHours(1));
                }
            }
        } catch (\Exception $e) {
            return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['offers' => $offers], Response::HTTP_OK, null);
    }

    public function getOffer($id)
    {
        try {
            $offer = Offer::with('images')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['offer' => $offer], Response::HTTP_OK, null);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'isActive' => 'boolean',
            'newImages' => 'array',
            'newImages.*.base64' => 'required',
        ];

        $messages = [
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'isActive.boolean' => 'validation.requiredField',
            'newImages.required' => 'validation.requiredField',
            'newImages.*.base64' => [
                'required' => 'validation.requiredField',
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $offer = new Offer();
        $offer->title = $request->title;
        $offer->description = $request->description;
        $offer->isActive = $request->isActive;
        $created = $offer->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $path = 'offers/' . $offer->id . '/';
        $imageErrors = ImageController::store($request->newImages, $path, $offer);

        return $this->response(true, 'recordAdded', [], Response::HTTP_OK, $imageErrors);
    }

    public function mark($id)
    {
        try {
            $offer = Offer::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $offer->isActive = !$offer->isActive;
        $updated = $offer->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, null);
    }

    public function delete($id)
    {
        try {
            $offer = Offer::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $deleted = $offer->delete();

        if (!$deleted) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        ImageController::deleteAllImages('offer', $id);

        return $this->response(true, 'recordRemoved', [], Response::HTTP_OK, null);
    }

    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:offers',
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'isActive' => 'boolean',
            'newImages' => 'array',
            'newImages.*.base64' => 'required',
            'storedImages' => 'array',
            'storedImages.*.id' => 'required|exists:images',
            'storedImages.*.path' => 'required',
            'storedImages.*.name' => 'required',
            'deletedImages' => 'array',
            'deletedImages.*.id' => 'required|exists:images',
            'deletedImages.*.path' => 'required',
            'deletedImages.*.name' => 'required',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'isActive.boolean' => 'validation.requiredField',
            'newImages.required' => 'validation.requiredField',
            'newImages.*.base64' => [ 'required' => 'validation.requiredField' ],
            'storedImages.required' => 'validation.storedImagesDoesNotExist',
            'storedImages.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'validation.storedImagesIdentifierDoesNotExist'
            ],
            'storedImages.*.path' => [ 'required' => 'validation.requiredField' ],
            'storedImages.*.name' => [ 'required' => 'validation.requiredField' ],
            'deletedImages.required' => 'validation.deletedImagesDoesNotExist',
            'deletedImages.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'validation.deletedImagesIdentifierDoesNotExist'
            ],
            'deletedImages.*.path' => [ 'required' => 'validation.requiredField' ],
            'deletedImages.*.name' => [ 'required' => 'validation.requiredField' ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        $dateTime = Carbon::now();

        $offer = Offer::find($request->id);
        $offer->title = $request->title;
        $offer->description = $request->description;
        $offer->isActive = $request->isActive;
        $offer->editedAt = $dateTime->toDateTimeString();
        $updated = $offer->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $path = 'offers/' . $offer->id . '/';
        ImageController::deleteImages($request->deletedImages, $path);
        $imageErrors = ImageController::store($request->newImages, $path, $offer);

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, $imageErrors);
    }
}
