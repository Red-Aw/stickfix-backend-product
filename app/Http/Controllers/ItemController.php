<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Item;

use App\Http\Controllers\EnumController;
use App\Http\Controllers\ImageController;

use Carbon\Carbon;

class ItemController extends Controller
{
    private $enumController;

    public function __construct(EnumController $enumController)
    {
        $this->enumController = $enumController;
    }

    public function getItems($isAuth)
    {
        try {
            if ($isAuth) {
                $items = Item::with('images')->orderBy('createdAt', 'desc')->get();
            } else {
                if (Cache::has('getItemsGuest')) {
                    $items = Cache::get('getItemsGuest');
                } else {
                    $items = Item::with('images')->where('isActive', true)->orderBy('createdAt', 'desc')->get();
                    Cache::put('getItemsGuest', $items, Carbon::now()->addMinutes(5));
                }
            }
        } catch (\Exception $e) {
            return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['items' => $items], Response::HTTP_OK, null);
    }

    public function getItem($id)
    {
        try {
            $item = Item::with('images')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['item' => $item], Response::HTTP_OK, null);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'category' => 'required',
            'isActive' => 'boolean',
            'state' => 'required',
            'brand' => 'nullable',
            'size' => 'nullable',
            'stickFlex' => 'nullable',
            'bladeCurve' => 'nullable',
            'bladeSide' => 'nullable',
            'stickSize' => 'nullable',
            'skateLength' => 'nullable',
            'skateWidth' => 'nullable',
            'newImages' => 'array',
            'newImages.*.base64' => 'required',
        ];

        $messages = [
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbers',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'category.required' => 'validation.requiredField',
            'isActive.boolean' => 'validation.requiredField',
            'state.required' => 'validation.requiredField',
            'newImages.required' => 'validation.requiredField',
            'newImages.*.base64' => [
                'required' => 'validation.requiredField',
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        if (!$this->isValidPrice($request->price)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'price' => ['validation.mayContainOnlyNaturalNumbers'] ],
            );
        }

        if (!$this->isValidCategory($request->category)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'category' => ['validation.requiredField'] ],
            );
        }

        if (!$this->isValidState($request->state)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'state' => ['validation.requiredField'] ],
            );
        }

        $brandKey = null;
        if ($request->brand) {
            $brandKey = $request->brand;

            if (!$this->isValidBrand($request->brand)) {
                return $this->response(
                    false,
                    'error.validationError',
                    [],
                    Response::HTTP_OK,
                    [ 'brand' => ['validation.requiredField'] ],
                );
            }
        }

        $itemSizes = self::getItemSizes(
            $request->category,
            array(
                'size' => $request->size,
                'stickSize' => $request->stickSize,
                'stickFlex' => $request->stickFlex,
                'bladeCurve' => $request->bladeCurve,
                'bladeSide' => $request->bladeSide,
                'skateLength' => $request->skateLength,
                'skateWidth' => $request->skateWidth,
            ),
        );

        $item = new Item;
        $item->title = $request->title;
        $item->description = $request->description;
        $item->priceInMinorUnit = intval($request->price * 100);
        $item->category = $request->category;
        $item->isActive = $request->isActive;
        $item->state = $request->state;
        $item->brand = $brandKey;
        $item->size = $itemSizes['size'];
        $item->stickSize = $itemSizes['stickSize'];
        $item->stickFlex = $itemSizes['stickFlex'];
        $item->bladeCurve = $itemSizes['bladeCurve'];
        $item->bladeSide = $itemSizes['bladeSide'];
        $item->skateLength = $itemSizes['skateLength'];
        $item->skateWidth = $itemSizes['skateWidth'];
        $created = $item->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $path = 'items/' . $item->id . '/';
        $imageErrors = ImageController::store($request->newImages, $path, $item);

        return $this->response(true, 'recordAdded', [], Response::HTTP_OK, $imageErrors);
    }

    public function mark($id)
    {
        try {
            $item = Item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $item->isActive = !$item->isActive;
        $updated = $item->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, null);
    }

    public function delete($id)
    {
        try {
            $item = Item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $deleted = $item->delete();

        if (!$deleted) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        ImageController::deleteAllImages('item', $id);

        return $this->response(true, 'recordRemoved', [], Response::HTTP_OK, null);
    }

    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:items',
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'category' => 'required',
            'isActive' => 'boolean',
            'state' => 'required',
            'brand' => 'nullable',
            'size' => 'nullable',
            'stickFlex' => 'nullable',
            'bladeCurve' => 'nullable',
            'bladeSide' => 'nullable',
            'stickSize' => 'nullable',
            'skateLength' => 'nullable',
            'skateWidth' => 'nullable',
            'newImages' => 'array',
            'newImages.*.base64' => 'required',
            'storedImages' => 'array',
            'storedImages.*.id' => 'required|exists:images',
            'storedImages.*.path' => 'required',
            'storedImages.*.name' => 'required',
            'deletedImages' => 'array',
            'deletedImages.*.id' => 'required|exists:images',
            'deletedImages.*.path' => 'required',
            'deletedImages.*.name' => 'required',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbers',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'category.required' => 'validation.requiredField',
            'isActive.boolean' => 'validation.requiredField',
            'state.required' => 'validation.requiredField',
            'newImages.required' => 'validation.requiredField',
            'newImages.*.base64' => [ 'required' => 'validation.requiredField' ],
            'storedImages.required' => 'validation.storedImagesDoesNotExist',
            'storedImages.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'storedImagesIdentifierDoesNotExist'
            ],
            'storedImages.*.path' => [ 'required' => 'validation.requiredField' ],
            'storedImages.*.name' => [ 'required' => 'validation.requiredField' ],
            'deletedImages.required' => 'validation.deletedImagesDoesNotExist',
            'deletedImages.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'deletedImagesIdentifierDoesNotExist'
            ],
            'deletedImages.*.path' => [ 'required' => 'validation.requiredField' ],
            'deletedImages.*.name' => [ 'required' => 'validation.requiredField' ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        if (!$this->isValidPrice($request->price)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'price' => ['validation.mayContainOnlyNaturalNumbers'] ],
            );
        }

        if (!$this->isValidCategory($request->category)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'category' => ['validation.requiredField'] ],
            );
        }

        if (!$this->isValidState($request->state)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'state' => ['validation.requiredField'] ],
            );
        }

        $brandKey = null;
        if ($request->brand) {
            $brandKey = $request->brand;

            if (!$this->isValidBrand($request->brand)) {
                return $this->response(
                    false,
                    'error.validationError',
                    [],
                    Response::HTTP_OK,
                    [ 'brand' => ['validation.requiredField'] ],
                );
            }
        }

        $itemSizes = self::getItemSizes(
            $request->category,
            array(
                'size' => $request->size,
                'stickSize' => $request->stickSize,
                'stickFlex' => $request->stickFlex,
                'bladeCurve' => $request->bladeCurve,
                'bladeSide' => $request->bladeSide,
                'skateLength' => $request->skateLength,
                'skateWidth' => $request->skateWidth,
            ),
        );

        $dateTime = Carbon::now();

        $item = Item::find($request->id);
        $item->title = $request->title;
        $item->description = $request->description;
        $item->priceInMinorUnit = intval($request->price * 100);
        $item->category = $request->category;
        $item->isActive = $request->isActive;
        $item->state = $request->state;
        $item->brand = $brandKey;
        $item->size = $itemSizes['size'];
        $item->stickSize = $itemSizes['stickSize'];
        $item->stickFlex = $itemSizes['stickFlex'];
        $item->bladeCurve = $itemSizes['bladeCurve'];
        $item->bladeSide = $itemSizes['bladeSide'];
        $item->skateLength = $itemSizes['skateLength'];
        $item->skateWidth = $itemSizes['skateWidth'];
        $item->editedAt = $dateTime->toDateTimeString();
        $updated = $item->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        $path = 'items/' . $item->id . '/';
        ImageController::deleteImages($request->deletedImages, $path);
        $imageErrors = ImageController::store($request->newImages, $path, $item);

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, $imageErrors);
    }

    public static function isValidPrice($price)
    {
        return is_float(floatval($price));
    }

    private function getItemSizes($categoryValue, $providedSizes)
    {
        $itemSizes = array(
            'size' => null,
            'stickSize' => null,
            'stickFlex' => null,
            'bladeCurve' => null,
            'bladeSide' => null,
            'skateLength' => null,
            'skateWidth' => null,
        );

        if ($categoryValue === 'SKATES') {
            $itemSizes['skateWidth'] = $providedSizes['skateWidth'];

            foreach ($this->enumController->getEnums('SKATES.LENGTH') as $key => $size) {
                if (($key . ' - ' . 'EUR ' . $size['EUR']) === $providedSizes['skateLength']) {
                    $itemSizes['skateLength'] = $key;
                }
            }
        } else if ($categoryValue === 'STICKS') {
            $itemSizes['stickSize'] = $providedSizes['stickSize'];
            $itemSizes['stickFlex'] = $providedSizes['stickFlex'];
            $itemSizes['bladeCurve'] = $providedSizes['bladeCurve'];
            $itemSizes['bladeSide'] = $providedSizes['bladeSide'];
        } else if ($categoryValue === 'PANTS'
            || $categoryValue === 'SHOULDER_PADS'
            || $categoryValue === 'ELBOW_PADS'
        ) {
            $itemSizes['size'] = $providedSizes['size']['value'];
        } else {
            $itemSizes['size'] = $providedSizes['size'];
        }

        return $itemSizes;
    }

    private function isValidState($chosenState)
    {
        foreach ($this->enumController->getEnums('STATES') as $state) {
            if ($chosenState === $state) {
                return true;
            }
        }

        return false;
    }

    private function isValidCategory($chosenCategory)
    {
        foreach ($this->enumController->getEnums('CATEGORIES') as $category) {
            if ($chosenCategory === $category) {
                return true;
            }
        }

        return false;
    }

    private function isValidBrand($chosenBrand)
    {
        foreach ($this->enumController->getEnums('BRANDS') as $brand) {
            if ($chosenBrand === $brand) {
                return true;
            }
        }

        return false;
    }
}
