<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Service;

use App\Http\Controllers\EnumController;
use App\Http\Controllers\ItemController;

use Carbon\Carbon;

class ServiceController extends Controller
{
    private $enumController;

    public function __construct(EnumController $enumController)
    {
        $this->enumController = $enumController;
    }

    public function getServices($isAuth)
    {
        try {
            if ($isAuth) {
                $services = Service::orderBy('createdAt', 'asc')->get();
            } else {
                if (Cache::has('getServicesGuest')) {
                    $services = Cache::get('getServicesGuest');
                } else {
                    $services = Service::where('isActive', true)->orderBy('createdAt', 'asc')->get();
                    Cache::put('getServicesGuest', $services, Carbon::now()->addMinutes(5));
                }
            }
        } catch (\Exception $e) {
            return $this->response(false, 'error.errorSelectingData', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['services' => $services], Response::HTTP_OK, null);
    }

    public function getService($id)
    {
        try {
            $service = Service::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        }

        return $this->response(true, '', ['service' => $service], Response::HTTP_OK, null);
    }

    public function store(Request $request)
    {
        $rules = [
            'serviceType' => 'required',
            'title' => 'required|min:3|max:256',
            'note' => 'min:3|max:256|nullable',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'isActive' => 'boolean',
        ];

        $messages = [
            'serviceType.required' => 'validation.requiredField',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'note.min' => 'validation.mustBeAtLeast3CharsLong',
            'note.max' => 'validation.mustBe256CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbers',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'isActive.boolean' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        if (!ItemController::isValidPrice($request->price)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'price' => ['validation.mayContainOnlyNaturalNumbers'] ],
            );
        }

        if (!$this->isValidServiceType($request->serviceType)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'serviceType' => ['validation.requiredField'] ],
            );
        }

        $service = new Service;
        $service->serviceType = $request->serviceType;
        $service->title = $request->title;
        $service->note = $request->note;
        $service->priceInMinorUnit = intval($request->price * 100);
        $service->isActive = $request->isActive;
        $created = $service->save();

        if (!$created) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'recordAdded', [], Response::HTTP_OK, null);
    }

    public function mark($id)
    {
        try {
            $service = Service::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $service->isActive = !$service->isActive;
        $updated = $service->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, null);
    }

    public function delete($id)
    {
        try {
            $service = Service::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->response(false, 'error.recordNotFound', [], Response::HTTP_OK, null);
        } catch (\Exception $e) {
            return $this->response(false, 'error.error', [], Response::HTTP_OK, null);
        }

        $deleted = $service->delete();

        if (!$deleted) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'recordRemoved', [], Response::HTTP_OK, null);
    }


    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:services',
            'serviceType' => 'required',
            'title' => 'required|min:3|max:256',
            'note' => 'min:3|max:256|nullable',
            'price' => 'numeric|min:0|max:2000000000|nullable',
            'isActive' => 'boolean',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'serviceType.required' => 'validation.requiredField',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'note.min' => 'validation.mustBeAtLeast3CharsLong',
            'note.max' => 'validation.mustBe256CharsOrFewer',
            'price.numeric' => 'validation.mayContainOnlyNaturalNumbers',
            'price.min' => 'validation.mustBeAPositiveValue',
            'price.max' => 'validation.numberTooLarge',
            'isActive.boolean' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $this->response(false, 'error.validationError', [], Response::HTTP_OK, $validator->errors());
        }

        if (!ItemController::isValidPrice($request->price)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'price' => ['validation.mayContainOnlyNaturalNumbers'] ],
            );
        }

        if (!$this->isValidServiceType($request->serviceType)) {
            return $this->response(
                false,
                'error.validationError',
                [],
                Response::HTTP_OK,
                [ 'serviceType' => ['validation.requiredField'] ],
            );
        }

        $dateTime = Carbon::now();

        $service = Service::find($request->id);
        $service->serviceType = $request->serviceType;
        $service->title = $request->title;
        $service->note = $request->note;
        $service->priceInMinorUnit = intval($request->price * 100);
        $service->isActive = $request->isActive;
        $service->editedAt = $dateTime->toDateTimeString();
        $updated = $service->save();

        if (!$updated) {
            return $this->response(false, 'error.databaseError', [], Response::HTTP_OK, null);
        }

        return $this->response(true, 'dataUpdated', [], Response::HTTP_OK, null);
    }

    private function isValidServiceType($chosenServiceType)
    {
        foreach ($this->enumController->getEnums('SERVICE_TYPES') as $serviceType) {
            if ($chosenServiceType === $serviceType) {
                return true;
            }
        }

        return false;
    }
}
