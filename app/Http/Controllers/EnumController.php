<?php

namespace App\Http\Controllers;

use App\Services\EnumService;

class EnumController extends Controller
{
    private $enumService;

    public function __construct(EnumService $enumService)
    {
        $this->enumService = $enumService;
    }

    public function getAllEnums()
    {
        return $this->enumService->getAllEnums();
    }

    public function getEnums($enumType)
    {
        $enumResponse = json_decode($this->getAllEnums(), true);
        $enums = [];
        if (is_array($enumResponse) && array_key_exists('data', $enumResponse)) {
            $enumResponse = $enumResponse['data'];
            if ($enumType === 'SKATES.LENGTH') {
                if (is_array($enumResponse) && array_key_exists('SIZES', $enumResponse)) {
                    if (is_array($enumResponse['SIZES']) && array_key_exists('SKATES', $enumResponse['SIZES'])) {
                        if (is_array($enumResponse['SIZES']['SKATES']) && array_key_exists('LENGTH', $enumResponse['SIZES']['SKATES'])) {
                            $enums = $enumResponse['SIZES']['SKATES']['LENGTH'];
                        }
                    }
                }
            } else {
                if (is_array($enumResponse) && array_key_exists($enumType, $enumResponse)) {
                    $enums = $enumResponse[$enumType];
                }
            }
        }

        return $enums;
    }
}
