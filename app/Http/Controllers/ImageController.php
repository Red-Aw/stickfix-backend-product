<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\Image;
use App\Models\Offer;
use App\Models\Item;
use Carbon\Carbon;

class ImageController extends Controller
{
    public static function store($images, $path, $object)
    {
        if ($object instanceof Offer) {
            $offerId = $object->id;
            $itemId = 0;
        } else if ($object instanceof Item) {
            $offerId = 0;
            $itemId = $object->id;
        } else {
            return [ 'newImages' => [ 'Wrong object' ] ];
        }

        $dateTime = Carbon::now();
        $errorList = [ 'newImages' => [] ];

        if (!is_array($images)) {
            return $errorList;
        }

        foreach ($images as $key => $img) {
            if (!empty($img) && !empty($img['base64'])) {
                $imageFile = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img['base64']));
                if ($imageFile === false) {
                    $errorList['newImages'][] = [ $key => 'validation.errorAddingImage' ];
                    continue;
                }

                try {
                    $imageExt = explode('/', mime_content_type($img['base64']))[1];
                } catch (\Exception $e) {
                    $errorList['newImages'][] = [ $key => 'validation.errorAddingImage' ];
                    continue;
                }

                if (!in_array($imageExt, [ 'png', 'jpeg', 'jpg', 'bmp' ])) {
                    $errorList['newImages'][] = [ $key => 'validation.incorrectImageFormatAllowed' ];
                    continue;
                }

                // Encoded data is about 33% heavier. Check mark - 9 MB
                if (strlen($imageFile) > 9000000) {
                    $errorList['newImages'][] = [ $key => 'validation.imageSizeMustBeLessThan5MB' ];
                    continue;
                }

                $imageName = $key . '_' . $dateTime->timestamp . '.' . $imageExt;

                Storage::disk('uploads')->put($path . $imageName, $imageFile);
                $image = new Image();
                $image->itemId = $itemId;
                $image->offerId = $offerId;
                $image->name = $imageName;
                $image->path = './uploads/' . $path;
                $created = $image->save();

                if (!$created) {
                    $errorList['newImages'][] = [ $key => 'validation.errorAddingImage' ];
                    continue;
                }
            }
        }

        return $errorList;
    }

    public static function deleteImages($images, $path)
    {
        if (!is_array($images)) {
            return;
        }

        foreach ($images as $img) {
            if (!empty($img) && !empty($img['id']) && !empty($img['path']) && !empty($img['name'])) {
                Storage::disk('uploads')->delete($path . \strval($img['name']));
                Image::destroy(\intval($img['id']));
            }
        }
    }

    public static function deleteAllImages($model, $id)
    {
        if ($model === 'item') {
            $modelId = 'itemId';
            $folder = 'items';
        } else if ($model === 'offer') {
            $modelId = 'offerId';
            $folder = 'offers';
        } else {
            return;
        }

        $imageIds = [];
        $images = Image::where($modelId, $id)->get();
        foreach ($images as $img) {
            $imageIds[] = $img['id'];
        }
        if (count($imageIds)) {
            Image::destroy($imageIds);
        }
        Storage::disk('uploads')->deleteDirectory($folder . '/' . $id);
    }
}
