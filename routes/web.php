<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/getItems/{isAuth}', 'ItemController@getItems');
    $router->get('/getItem/{id}', 'ItemController@getItem');
    $router->post('/createItem', 'ItemController@store');
    $router->get('/markItem/{id}', 'ItemController@mark');
    $router->get('/deleteItem/{id}', 'ItemController@delete');
    $router->post('/editItem', 'ItemController@edit');

    $router->get('/getServices/{isAuth}', 'ServiceController@getServices');
    $router->get('/getService/{id}', 'ServiceController@getService');
    $router->post('/createService', 'ServiceController@store');
    $router->get('/markService/{id}', 'ServiceController@mark');
    $router->get('/deleteService/{id}', 'ServiceController@delete');
    $router->post('/editService', 'ServiceController@edit');

    $router->get('/getOffers/{isAuth}', 'OfferController@getOffers');
    $router->get('/getOffer/{id}', 'OfferController@getOffer');
    $router->post('/createOffer', 'OfferController@store');
    $router->get('/markOffer/{id}', 'OfferController@mark');
    $router->get('/deleteOffer/{id}', 'OfferController@delete');
    $router->post('/editOffer', 'OfferController@edit');
});
