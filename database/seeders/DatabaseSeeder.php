<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Hokeja nūjas remonts',
            'note' => 'Labojam TIKAI karbona nūjas',
            'priceInMinorUnit' => 2000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Hokeja nūjas pagarināšana vai samazināšana',
            'note' => null,
            'priceInMinorUnit' => 1000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Hokeja aprīkojuma labošana',
            'note' => 'Atkarīgs no aprīkojuma!',
            'priceInMinorUnit' => 2000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Slidu asināšana',
            'note' => 'Asinām ar elipses un trapecveida asināmo!',
            'priceInMinorUnit' => 500,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Kapes izveidošana <span class="blue--text text--lighten-1">Patstāvīgiem klientiem atlaide!</span>',
            'note' => 'Kape pielāgota speciāli Jūsu zobiem!',
            'priceInMinorUnit' => 1000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam salauztas vai bojātas karbona nūjas',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam Jūsu veco vai nevajadzīgo hokeja aprīkojumu',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam arī citas ar hokeju saistītas lietas',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam daiļslidošanas slidas',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('offers')->insert([
            'title' => 'Hokeja nūju un aprīkojuma labošana',
            'description' => 'Piedāvājam hokeja nūju labošanu. Labošana norit ar karbonšķiedru materiāliem un tehnoloģijām. Piedāvājam hokeja aprīkojuma labošanu. Labojam cimdus, plecsargus un citas aprīkojuma daļas',
            'isActive' => true,
        ]);

        DB::table('offers')->insert([
            'title' => 'Pārdoda hokeja nūjas',
            'description' => 'Pārdodam gan lietotas, gan jaunas hokeja nūjas. Spēlētājiem un vārtsargiem uz abām spēlēšanas pusēm. Piedāvājam arī nūju pagarināšanas pakalpojumu',
            'isActive' => true,
        ]);

        DB::table('offers')->insert([
            'title' => 'Pārdoda hokeja aprīkojumu',
            'description' => 'Pārdodam lietotu un jaunu hokeja aprīkojumu dažādām vecuma grupām. Piedāvājam izveidot kapes',
            'isActive' => true,
        ]);
    }
}
