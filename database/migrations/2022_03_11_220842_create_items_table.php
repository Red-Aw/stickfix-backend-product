<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('title', 256);
            $table->text('description')->default('')->nullable();
            $table->integer('priceInMinorUnit')->nullable();
            $table->enum('category', ['SKATES', 'STICKS', 'HELMET', 'GLOVES', 'PANTS', 'SHOULDER_PADS', 'SHIN_GUARDS', 'ELBOW_PADS', 'BAGS', 'WEAR_AND_ACCESSORIES', 'KITS', 'KIDS_EQUIPMENT', 'GOALIES_EQUIPMENT', 'FIGURE_SKAITING', 'INLINE', 'OTHER']);
            $table->boolean('isActive')->default(1);
            $table->enum('state', ['NEW', 'USED', 'SLIGHTLY_USED']);
            $table->enum('brand', ['NO', 'BAUER', 'CCM', 'EASTON', 'JOFA', 'MISSION', 'NIKE', 'RBK', 'TACKLA', 'TRUE', 'VAUGHN', 'WARRIOR', 'OTHER',])->nullable();
            $table->string('size', 32)->nullable();
            $table->string('stickSize', 32)->nullable();
            $table->string('stickFlex', 32)->nullable();
            $table->string('bladeCurve', 32)->nullable();
            $table->string('bladeSide', 32)->nullable();
            $table->string('skateLength', 32)->nullable();
            $table->string('skateWidth', 32)->nullable();
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    public function down()
    {
        Schema::dropIfExists('items');
    }
}
