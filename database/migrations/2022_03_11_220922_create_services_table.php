<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->enum('serviceType', ['OFFER', 'BUY']);
            $table->string('title', 256);
            $table->string('note', 256)->nullable();
            $table->integer('priceInMinorUnit')->nullable();
            $table->boolean('isActive')->default(1);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    public function down()
    {
        Schema::dropIfExists('services');
    }
}
