<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('title', 256);
            $table->text('description')->default('')->nullable();
            $table->boolean('isActive')->default(1);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
