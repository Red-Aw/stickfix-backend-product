<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class ItemFactory extends Factory
{
    protected $model = Item::class;

    public function definition(): array
    {
    	return [
            'title' => 'Perpetual',
            'description' => 'Perpetual sincerity out suspected necessary one but provision satisfied. Respect nothing use set waiting pursuit nay you looking. If on prevailed concluded ye abilities. Address say you new but minuter greater. Do denied agreed in innate. Can and middletons thoroughly themselves him. Tolerably sportsmen belonging in september no am immediate newspaper. Theirs expect dinner it pretty indeed having no of. Principle september she conveying did eat may extensive.',
            'priceInMinorUnit' => 10000,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'NO',
            'size' => null,
            'stickSize' => null,
            'stickFlex' => null,
            'bladeCurve' => null,
            'bladeSide' => null,
            'skateLength' => '1.0',
            'skateWidth' => 'C',
            'createdAt' => Carbon::now(),
            'editedAt' => Carbon::now(),
    	];
    }
}
