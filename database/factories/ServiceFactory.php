<?php

namespace Database\Factories;

use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class ServiceFactory extends Factory
{
    protected $model = Service::class;

    public function definition(): array
    {
    	return [
            'serviceType' => 'OFFER',
            'title' => 'Two exquisite objection delighted',
            'note' => 'Two exquisite objection delighted deficient yet its contained. Cordial because are account evident its subject but eat.',
            'priceInMinorUnit' => 800,
            'isActive' => 1,
            'createdAt' => Carbon::now(),
            'editedAt' => Carbon::now(),
    	];
    }
}
