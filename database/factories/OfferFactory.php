<?php

namespace Database\Factories;

use App\Models\Offer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class OfferFactory extends Factory
{
    protected $model = Offer::class;

    public function definition(): array
    {
    	return [
            'title' => 'Rendered her for put improved',
            'description' => 'Rendered her for put improved concerns his. Ladies bed wisdom theirs mrs men months set. Everything so dispatched as it increasing pianoforte',
            'isActive' => 1,
            'createdAt' => Carbon::now(),
            'editedAt' => Carbon::now(),
    	];
    }
}
