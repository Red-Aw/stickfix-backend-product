<?php

use App\Models\Service;

class ServiceTest extends TestCase
{
	public function testGetOffersUnauthorized()
    {
        $serviceData = [
            'serviceType' => 'BUY',
            'title' => 'Seen you eyes son show',
            'note' => 'Far two unaffected one alteration apartments',
            'priceInMinorUnit' => 2500,
            'isActive' => 1,
        ];
        \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/getServices/1', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

	public function testGetOffers()
    {
        $serviceData = [
            'serviceType' => 'BUY',
            'title' => 'Seen you eyes son show',
            'note' => 'Far two unaffected one alteration apartments',
            'priceInMinorUnit' => 3000,
            'isActive' => 1,
        ];
        \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/getServices/1', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testGetServiceUnauthorized()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'Of incommode supported provision',
            'note' => 'on furnished objection exquisite me',
            'priceInMinorUnit' => 2000,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/getService/' . $serviceObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

	public function testGetService()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'Of incommode supported provision',
            'note' => 'on furnished objection exquisite me',
            'priceInMinorUnit' => 2000,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/getService/' . $serviceObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testCreateServiceUnauthorized()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'Him boisterous invitation',
            'note' => 'im boisterous invitation dispatched had connection inhabiting projection.',
            'price' => 5.00,
            'isActive' => 1,
        ];

        $this->json('POST', 'api/createService', $serviceData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testCreateService()
    {
        $serviceData = [
            'serviceType' => 'BUY',
            'title' => 'Sitting mistake towards his few country ask',
            'note' => 'Sitting mistake towards his few country ask. You delighted two rapturous six depending objection happiness something the.',
            'price' => 4.00,
            'isActive' => 1,
        ];

        $this->json('POST', 'api/createService', $serviceData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testEditServiceUnauthorized()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'Forfeited you engrossed',
            'note' => 'conduct at an replied removal an amongst. Remaining determine few her two cordially admitting old.',
            'priceInMinorUnit' => 400,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $editedServiceData = [
            'id' => $serviceObj->id,
            'serviceType' => 'OFFER',
            'title' => 'Advice me cousin an sprin',
            'note' => 'Tiled man stand tears ten joy there terms any widen. Procuring continued suspicion its ten.',
            'price' => 8.00,
            'isActive' => 1,
        ];

        $this->json('POST', 'api/editService', $editedServiceData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testEditService()
    {
        $serviceData = [
            'serviceType' => 'BUY',
            'title' => 'Up is opinion message',
            'note' => 'Up is opinion message manners correct hearing husband my. Disposing commanded dashwoods cordially depending at at',
            'priceInMinorUnit' => 800,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $editedServiceData = [
            'id' => $serviceObj->id,
            'serviceType' => 'BUY',
            'title' => 'Its strangers who you certainty',
            'note' => '',
            'price' => 9.00,
            'isActive' => 0,
        ];

		$this->json('POST', 'api/editService', $editedServiceData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testMarkServiceUnauthorized()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'By so delight of showing neither',
            'note' => 'Deal sigh up in shew away when. Pursuit express no or prepare replied',
            'priceInMinorUnit' => 1000,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/markService/' . $serviceObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testMarkService()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'When be draw drew ye',
            'note' => 'Defective in do recommend suffering.',
            'priceInMinorUnit' => 999,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/markService/' . $serviceObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testDeleteServiceUnauthorized()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'no purse as fully me or point',
            'note' => 'indness own whatever betrayed her moreover procured replying for and',
            'priceInMinorUnit' => 1200,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/deleteService/' . $serviceObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testDeleteService()
    {
        $serviceData = [
            'serviceType' => 'OFFER',
            'title' => 'indulged no do',
            'note' => 'Covered ten nor comfort offices carried',
            'priceInMinorUnit' => 1500,
            'isActive' => 1,
        ];
        $serviceObj = \App\Models\Service::factory()->create($serviceData);

        $this->json('GET', 'api/deleteService/' . $serviceObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	private function clearServiceTableAtEnd()
    {
        Service::truncate();
    }
}
