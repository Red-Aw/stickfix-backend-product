<?php

use App\Models\Item;
use App\Models\Image;

class ItemTest extends TestCase
{

    public function testGetItemsUnauthorized()
    {
        $firstFactoryItemData = [
            'title' => 'Dispatched entreaties',
            'description' => 'Dispatched entreaties boisterous say why stimulated. Certain forbade picture now prevent carried she get see sitting. Up twenty limits as months. Inhabit so perhaps of in to certain. Sex excuse chatty was seemed warmth. Nay add far few immediate sweetness earnestly dejection.',
            'priceInMinorUnit' => 3999,
            'category' => 'HELMET',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'CCM',
            'size' => 'XS',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        \App\Models\Item::factory()->create($firstFactoryItemData);

        $secondFactoryItemData = [
            'title' => 'Old unsatiable',
            'description' => 'Old unsatiable our now but considered travelling impression. In excuse hardly summer in basket misery. By rent an part need. At wrong of of water those linen. Needed oppose seemed how all. Very mrs shed shew gave you. Oh shutters do removing reserved wandered an. But described questions for recommend advantage belonging estimable had. Pianoforte reasonable as so am inhabiting. Chatty design remark and his abroad figure but its.',
            'priceInMinorUnit' => 13999,
            'category' => 'BAGS',
            'isActive' => 1,
            'state' => 'USED',
            'brand' => 'JOFA',
            'size' => 'S',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        \App\Models\Item::factory()->create($secondFactoryItemData);

        $this->json('GET', 'api/getItems/' . 1, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testGetItems()
    {
        $firstFactoryItemData = [
            'title' => 'entreaties Dispatched',
            'description' => 'entreaties boisterous. Certain forbade picture now prevent carried she get see sitting. Up twenty limits as months. Inhabit so perhaps of in to certain. Sex excuse chatty was seemed warmth. Nay add far few immediate sweetness earnestly dejection.',
            'priceInMinorUnit' => 2999,
            'category' => 'HELMET',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'CCM',
            'size' => 'XS',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        \App\Models\Item::factory()->create($firstFactoryItemData);

        $secondFactoryItemData = [
            'title' => 'unsatiable Old',
            'description' => 'Old impression. In excuse hardly summer in basket misery. By rent an part need. At wrong of of water those linen. Needed oppose seemed how all. Very mrs shed shew gave you. Oh shutters do removing reserved wandered an. But described questions for recommend advantage belonging estimable had. Pianoforte reasonable as so am inhabiting. Chatty design remark and his abroad figure but its.',
            'priceInMinorUnit' => 12999,
            'category' => 'BAGS',
            'isActive' => 0,
            'state' => 'USED',
            'brand' => 'CCM',
            'size' => 'S',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        \App\Models\Item::factory()->create($secondFactoryItemData);

        $this->json('GET', 'api/getItems/' . 1, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    public function testGetItemUnauthorized()
    {
        $factoryItemData = [
            'title' => 'Cause dried no solid',
            'description' => 'Cause dried no solid no an small so still widen. Ten weather evident smiling bed against she examine its. Rendered far opinions two yet moderate sex striking. Sufficient motionless compliment by stimulated assistance at. Convinced resolving extensive agreeable in it on as remainder. Cordially say affection met who propriety him. Are man she towards private weather pleased. In more part he lose need so want rank no. At bringing or he sensible pleasure. Prevent he parlors do waiting be females an message society.',
            'priceInMinorUnit' => 7500,
            'category' => 'STICKS',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => 120,
            'bladeCurve' => 'P88',
            'bladeSide' => 'NONE',
            'stickSize' => 'JR',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $this->json('GET', 'api/getItem/' . $item->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testGetItem()
    {
        $factoryItemData = [
            'title' => 'Cause dried no solid',
            'description' => 'Cause dried no solid no an small so still widen. Ten weather evident smiling bed against she examine its. Rendered far opinions two yet moderate sex striking. Sufficient motionless compliment by stimulated assistance at. Convinced resolving extensive agreeable in it on as remainder. Cordially say affection met who propriety him. Are man she towards private weather pleased. In more part he lose need so want rank no. At bringing or he sensible pleasure. Prevent he parlors do waiting be females an message society.',
            'priceInMinorUnit' => 7500,
            'category' => 'STICKS',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => 120,
            'bladeCurve' => 'P88',
            'bladeSide' => 'NONE',
            'stickSize' => 'JR',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $this->json('GET', 'api/getItem/' . $item->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    public function testStoreItemUnauthorized()
    {
        $itemData = [
            'title' => 'Mr oh winding it enjoyed by between',
            'description' => 'Mr oh winding it enjoyed by between. The servants securing material goodness her. Saw principles themselves ten are possession. So endeavor to continue cheerful doubtful we to. Turned advice the set vanity why mutual. Reasonably if conviction on be unsatiable discretion apartments delightful. Are melancholy appearance stimulated occasional entreaties end. Shy ham had esteem happen active county. Winding morning am shyness evident to. Garrets because elderly new manners however one village she.',
            'price' => 24.68,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'NO',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '1.0 - EUR 33.5',
            'skateWidth' => 'C',
            'images' => [],
        ];

        $this->json('POST', 'api/createItem', $itemData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testStoreItem()
    {
        $itemData = [
            'title' => 'Conveying or northward',
            'description' => 'Conveying or northward offending admitting perfectly my. Colonel gravity get thought fat smiling add but. Wonder twenty hunted and put income set desire expect. Am cottage calling my is mistake cousins talking up. Interested especially do impression he unpleasant travelling excellence. All few our knew time done draw ask.',
            'price' => 24.68,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'NO',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '1.5 - EUR 34',
            'skateWidth' => 'D',
            'images' => [],
        ];

        $this->json('POST', 'api/createItem', $itemData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    public function testEditItemUnauthorized()
    {
        $factoryItemData = [
            'title' => 'In alteration insipidity',
            'description' => 'In alteration insipidity impression by travelling reasonable up motionless. Of regard warmth by unable sudden garden ladies. No kept hung am size spot no. Likewise led and dissuade rejoiced welcomed husbands boy. Do listening on he suspected resembled. Water would still if to. Position boy required law moderate was may.',
            'priceInMinorUnit' => 2050,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '2.0 - EUR 35',
            'skateWidth' => 'E',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $editedItemData = [
            'id' => $item->id,
            'title' => 'When be draw drew',
            'description' => 'Defective in do recommend suffering. House it seven in spoil tiled court. Sister others marked fat missed did out use. Alteration possession dispatched collecting instrument travelling he or on. Snug give made at spot or late that mr. ',
            'price' => 30.50,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '2.0 - EUR 35',
            'skateWidth' => 'EE',
            'newImages' => [],
            'storedImages' => [],
            'deletedImages' => [],
        ];

        $this->json('POST', 'api/editItem', $editedItemData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testEditItem()
    {
        $factoryItemData = [
            'title' => 'Received overcame',
            'description' => 'Received overcame oh sensible so at an. Formed do change merely to county it. Am separate contempt domestic to to oh. On relation my so addition branched. Put hearing cottage she norland letters equally prepare too. Replied exposed savings he no viewing as up. Soon body add him hill. No father living really people estate if. Mistake do produce beloved demesne if am pursuit.',
            'priceInMinorUnit' => 5500,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '2.5 - EUR 35.5',
            'skateWidth' => 'D',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $editedItemData = [
            'id' => $item->id,
            'title' => 'Far curiosity',
            'description' => 'Far curiosity incommode now led smallness allowance. Favour bed assure son things yet. She consisted consulted elsewhere happiness disposing household any old the. Widow downs you new shade drift hopes small. So otherwise commanded sweetness we improving. Instantly by daughters resembled unwilling principle so middleton. Fail most room even gone her end like. Comparison dissimilar unpleasant six compliment two unpleasing any add. Ashamed my company thought wishing colonel it prevent he in. Pretended residence are something far engrossed old off.',
            'price' => 60.50,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '3.0 - EUR 36',
            'skateWidth' => 'R',
            'newImages' => [],
            'storedImages' => [],
            'deletedImages' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/editItem', $editedItemData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    public function testMarkItemUnauthorized()
    {
        $factoryItemData = [
            'title' => 'Affronting discretion',
            'description' => 'Affronting discretion as do is announcing. Now months esteem oppose nearer enable too six. She numerous unlocked you perceive speedily. Affixed offence spirits or ye of offices between. Real on shot it were four an as. Absolute bachelor rendered six nay you juvenile. Vanity entire an chatty to.',
            'priceInMinorUnit' => 100,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '3.5 - EUR 36.5',
            'skateWidth' => 'Fit 1',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $this->json('GET', 'api/markItem/' . $item->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testMarkItem()
    {
        $factoryItemData = [
            'title' => 'Six started',
            'description' => 'Six started far placing saw respect females old. Civilly why how end viewing attempt related enquire visitor. Man particular insensible celebrated conviction stimulated principles day. Sure fail or in said west. Right my front it wound cause fully am sorry if. She jointure goodness interest debating did outweigh. Is time from them full my gone in went. Of no introduced am literature excellence mr stimulated contrasted increasing. Age sold some full like rich new. Amounted repeated as believed in confined juvenile.',
            'priceInMinorUnit' => 2200,
            'category' => 'SKATES',
            'isActive' => 1,
            'state' => 'NEW',
            'brand' => 'EASTON',
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateLength' => '4.0 - EUR 37.5',
            'skateWidth' => 'Fit 2',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $this->json('GET', 'api/markItem/' . $item->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    public function testDeleteItemUnauthorized()
    {
        $factoryItemData = [
            'title' => 'Carriage quitting',
            'description' => 'Carriage quitting securing be appetite it declared. High eyes kept so busy feel call in. Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment. Passage weather as up am exposed. And natural related man subject. Eagerness get situation his was delighted.',
            'priceInMinorUnit' => 50,
            'category' => 'STICKS',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'BAUER',
            'size' => '',
            'stickFlex' => 100,
            'bladeCurve' => 'P92',
            'bladeSide' => 'LEFT',
            'stickSize' => 'SR',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $this->json('GET', 'api/deleteItem/' . $item->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);

        $this->clearServiceTableAtEnd();
    }

    public function testDeleteItemAsAuthenticated()
    {
        $factoryItemData = [
            'title' => 'Sussex result',
            'description' => 'Sussex result matter any end see. It speedily me addition weddings vicinity in pleasure. Happiness commanded an conveying breakfast in. Regard her say warmly elinor. Him these are visit front end for seven walls. Money eat scale now ask law learn. Side its they just any upon see last. He prepared no shutters perceive do greatest. Ye at unpleasant solicitude in companions interested.',
            'priceInMinorUnit' => 8000,
            'category' => 'STICKS',
            'isActive' => 1,
            'state' => 'SLIGHTLY_USED',
            'brand' => 'CCM',
            'size' => '',
            'stickFlex' => 110,
            'bladeCurve' => 'PM9',
            'bladeSide' => 'RIGHT',
            'stickSize' => 'INT',
            'skateLength' => '',
            'skateWidth' => '',
        ];
        $item = \App\Models\Item::factory()->create($factoryItemData);

        $this->json('GET', 'api/deleteItem/' . $item->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
            ]);

        $this->clearServiceTableAtEnd();
    }

    private function clearServiceTableAtEnd()
    {
        Item::truncate();
        Image::truncate();
    }
}
