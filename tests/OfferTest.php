<?php

use App\Models\Image;
use App\Models\Offer;

class OfferTest extends TestCase
{
    public function testGetOffersUnauthorized()
    {
        $offerData = [
            'title' => 'branched humanity',
            'description' => 'Ignorant branched humanity led now marianne too strongly entrance',
            'isActive' => 1,
        ];
        \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/getOffers/1', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

	public function testGetOffers()
    {
		$offerData = [
            'title' => 'design',
            'description' => 'design are dinner better nearer silent excuse',
            'isActive' => 1,
        ];
        \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/getOffers/1', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

    public function testGetOfferUnauthorized()
    {
        $offerData = [
            'title' => 'attending household',
            'description' => 'Middleton in objection discovery as agreeable',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/getOffer/' . $offerObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

	public function testGetOffer()
    {
        $offerData = [
            'title' => 'attending household',
            'description' => 'Middleton in objection discovery as agreeable',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/getOffer/' . $offerObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testCreateOfferUnauthorized()
    {
        $offerData = [
            'title' => 'earing now saw perhaps minutes',
            'description' => 'nstantly excellent therefore difficult he northward',
            'isActive' => 1,
            'images' => [],
        ];

        $this->json('POST', 'api/createOffer', $offerData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testCreateOffer()
    {
        $offerData = [
            'title' => 'Concerns greatest margaret',
            'description' => 'oor neat week do find past he. Be no surprise he honoured indulged',
            'isActive' => 1,
            'images' => [],
        ];

		$this->json('POST', 'api/createOffer', $offerData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testEditOfferUnauthorized()
    {
        $offerData = [
            'title' => 'allowance instantly strangers',
            'description' => 'applauded discourse so. Separate entrance welcomed sensible laughing why one moderate shy',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $editedOfferData = [
            'id' => $offerObj->id,
            'title' => 'We seeing piqued garden he',
            'description' => 'As in merry at forth least ye stood. And cold sons yet with',
            'isActive' => 1,
            'newImages' => [],
            'storedImages' => [],
            'deletedImages' => [],
        ];

        $this->json('POST', 'api/editOffer', $editedOfferData, [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testEditOffer()
    {
        $offerData = [
            'title' => 'epending up believing',
            'description' => 'Enough around remove to barton agreed regret in or it',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $editedOfferData = [
            'id' => $offerObj->id,
            'title' => 'shew come',
            'description' => 'Advantage mr estimable be commanded provision',
            'isActive' => 1,
            'newImages' => [],
            'storedImages' => [],
            'deletedImages' => [],
        ];

        $this->json('POST', 'api/editOffer', $editedOfferData, [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

    public function testMarkOfferUnauthorized()
    {
        $offerData = [
            'title' => 'Written enquire',
            'description' => 'On ye position greatest so desirous',
            'isActive' => 1,
        ];

        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/markOffer/' . $offerObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testMarkOffer()
    {
        $offerData = [
            'title' => 'comfort do written',
            'description' => 'elebrated contrasted discretion him sympathize her collecting occasional',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/markOffer/' . $offerObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	public function testDeleteOfferUnauthorized()
    {
        $offerData = [
            'title' => 'hough wished',
            'description' => 'waiting in on enjoyed placing it inquiry',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/deleteOffer/' . $offerObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
			->seeStatusCode(401);

		$this->clearServiceTableAtEnd();
    }

    public function testDeleteOfferAsAuthenticated()
    {
        $offerData = [
            'title' => 'diminution',
            'description' => 'we diminution preference thoroughly',
            'isActive' => 1,
        ];
        $offerObj = \App\Models\Offer::factory()->create($offerData);

        $this->json('GET', 'api/deleteOffer/' . $offerObj->id, [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
			->seeStatusCode(200)
			->seeJsonContains([
				'success' => true,
			]);

		$this->clearServiceTableAtEnd();
    }

	private function clearServiceTableAtEnd()
    {
        Offer::truncate();
		Image::truncate();
    }
}
