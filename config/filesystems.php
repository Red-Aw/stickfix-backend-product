<?php

return [
    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'uploads' => [
            'driver' => 'local',
            'root' => 'uploads',
            'url' => 'uploads',
        ],
    ],
];
